#include <chrono>
#include <iostream>

#include <cmath>
#include <cstdio>
#include <cstdlib>

#include <opencv2/opencv.hpp>
#include <opencv2/ximgproc.hpp>

int clampi(int val, int min, int max){
    if (val > max){
        return max;
    } else if (val < min){
        return min;
    } else {
        return val;
    }
}

void calcMean(cv::Mat &img, cv::Mat &mean, int wx, int wy){
    float mean_sum = 0;
    for (int wi = -wy; wi <= wy; wi++){
        for (int wj = -wx; wj <= wx; wj++){
            int indy = clampi(0+wi, 0, img.rows);
            int indx = clampi(0+wj, 0, img.cols);
            mean_sum += img.at<unsigned char>(indy, indx);
        }
    }
    mean.at<float>(0, 0) = mean_sum;
    for (int i = 0; i < img.rows; i++){
        if (i > 0){
            int subi = clampi(i-wy-1, 0, img.rows);
            int sumi = clampi(i+wy, 0, img.rows);
            mean_sum = mean.at<float>(i-1, 0);
            for (int wj = -wx; wj <= wx; wj++){
                int j = clampi(wj, 0, img.cols);
                mean_sum -= img.at<unsigned char>(subi, j);
                mean_sum += img.at<unsigned char>(sumi, j);
            }
            mean.at<float>(i, 0) = mean_sum;
        }
        for (int j = 1; j < img.cols; j++){
            mean_sum = mean.at<float>(i, j-1);
            int subj = clampi(j-wx-1, 0, img.cols);
            int sumj = clampi(j+wx, 0, img.cols);
            for (int wi = -wy; wi <= wy; wi++){
                int windi = clampi(i+wi, 0, img.rows);
                mean_sum -= img.at<unsigned char>(windi, subj);
                mean_sum += img.at<unsigned char>(windi, sumj); 
            }
            mean.at<float>(i, j) = mean_sum; // / ((wx+wx+1) * (wy+wy+1)*255);
        }
    } 
    mean = mean / ((wx+wx+1) * (wy+wy+1)*255);
}

void calcStdev(cv::Mat &img, cv::Mat &mean, cv::Mat &stdev, int wx, int wy){
    for (int i = 0; i < img.rows; i++){
        for (int j = 0; j < img.cols; j++){
            int stdev_sum = 0;
            for (int wi = -wy; wi <= wy; wi++){
                int indy = i+wi;
                if (indy < 0 || indy >= img.rows)
                    indy = i-wi;
                for (int wj = -wx; wj <= wx; wj++){
                    int indx = j+wj;
                    if (indx < 0 || indx >= img.cols)
                        indx = j-wj;
                    float val = img.at<unsigned char>(indy, indx) / 255.0f;
                    stdev_sum += (mean.at<float>(i, j) - val) * (mean.at<float>(i, j) - val);
                }
            }
            stdev.at<float>(i, j) = std::sqrt((float)stdev_sum / ((wx+wx+1) * (wy+wy+1)));
        }
    } 

}

void binarize(cv::Mat &img, cv::Mat &threshold, cv::Mat &binarized){
    for (int i = 0; i < img.rows; i++){
        for (int j = 0; j < img.cols; j++){
            if (img.at<unsigned char>(i, j)/255.0f < threshold.at<float>(i, j))
                binarized.at<unsigned char>(i, j) = 0;
            else
                binarized.at<unsigned char>(i, j) = 255;
        }
    }
} 

cv::Mat binarizeImg(cv::Mat &img, int wx, int wy, float k) {
    int rows = img.rows;
    int cols = img.cols;
    cv::Mat mean(rows, cols, CV_32FC1, cv::Scalar(0));
    cv::Mat stdev(rows, cols, CV_32FC1, cv::Scalar(0));
    cv::Mat threshold(rows, cols, CV_32FC1, cv::Scalar(0));
    cv::Mat binarized(rows, cols, CV_8UC1, cv::Scalar(0));

    calcMean(img, mean, wx, wy);
    calcStdev(img, mean, stdev, wx, wy);
    threshold = mean - k * stdev;
    binarize(img, threshold, binarized);

    return binarized;
}

int main(int argc, char** argv){
    if (argc != 3){
        printf("usage: %s <image_path> <window_size>\n", argv[0]);
        return -1;
    }

    int wx, wy;
    float k;
    cv::Mat img;
    img = cv::imread(argv[1], cv::IMREAD_GRAYSCALE);

    if (!img.data){
        printf("No img data \n");
        return -1;
    }
    wx = std::atoi(argv[2]);
    wy = wx;
    k = 0.3;

    int ntests = 1; 
    cv::Mat bin, bin_cv(img.rows, img.cols, CV_8UC1, cv::Scalar(0)),
        adiff(img.rows, img.cols, CV_8UC1, cv::Scalar(0));

    auto start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < ntests; i++){
        bin = binarizeImg(img, wx, wy, k);
        cv::ximgproc::niBlackThreshold(img, bin_cv, 255, CV_8UC1, (wx+wx+1), k);
        cv::absdiff(bin, bin_cv, adiff);
    }
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    printf("%ld microseconds\n", duration.count() / ntests);

    
    cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE);
    cv::imshow("Display Image", bin);
    cv::waitKey(0);

    cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE);
    cv::imshow("Display Image", bin_cv);
    cv::waitKey(0);

    cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE);
    cv::imshow("Display Image", adiff);
    cv::waitKey(0);

    cv::imwrite("bin.png", bin);
    cv::imwrite("bin_cv.png", bin_cv);
    cv::imwrite("bin_diff.png", adiff);;

    return 0;
}

